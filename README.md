## Dependencies
Running the source requires Pygame (http://pygame.org/news.html) and Python 2.

## Running the source
```shell
python2 main.py
```
or
```shell
python main.py
```

## Settings
Settings can be found in settings.py.

## Credits
The music was generated with [Autotracker](https://github.com/wibblymat/ld24/blob/master/autotracker.py).
The sound effects were generated with [Sfxr](http://www.drpetter.se/project_sfxr.html).
The Orbitron font was created by [The League of Moveable Type](https://www.theleagueofmoveabletype.com/).
