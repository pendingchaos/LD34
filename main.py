import time
import random
import math
import pygame

try:
    import pygame.gfxdraw
except ImportError:
    pass

from pygame.locals import *

# Not importing because I don't want to add imports into settings.py
exec(open("settings.py").read())

# "Abandon All Hope, Ye Who Enter Here"
# - xterm's README

TILE_NONE = 0
TILE_MAP = 1
TILE_COIN = 2
TILE_BADTHING = 3
TILE_POWERUP = 4
TILE_MEGACOIN = 5
TILE_MEGABADTHING = 6

BACKGROUND = 160
TILE_SIZE = 20
GRAVITY = 1
MAX_VEL = 40
PLAYER_JUMP_VEL = 1.5
PLAYER_SPEED = 4
MAX_PLAYER_HEALTH = 10

STATE_PLAY = "play"
STATE_MENU = "menu"
STATE_LOSE = "lose"
STATE_QUIT = "quit"

POWERUP_SWITCH = 0
POWERUP_COIN_GALORE = 1
POWERUP_INVINCIBLE = 2
POWERUP_FLY = 3
POWERUP_MEGACOIN = 4
POWERUP_MEGABADTHING = 5
POWERUP_MAX = 6

EVENT_DEACTIVATE_SWITCH = USEREVENT
EVENT_DEACTIVATE_COIN_GALORE = USEREVENT+1
EVENT_DEACTIVATE_INVINCIBLE = USEREVENT+2
EVENT_DEACTIVATE_FLY = USEREVENT+3
EVENT_DEACTIVATE_MEGACOIN = USEREVENT+4
EVENT_DEACTIVATE_MEGABADTHING = USEREVENT+5
EVENT_MUSIC_END = USEREVENT+6

WIDTH = WIDTH / TILE_SIZE * TILE_SIZE
HEIGHT = HEIGHT / TILE_SIZE * TILE_SIZE

coinSound = None
hitSound = None
jumpSound = None
loseSound = None
powerupSound = None
font = None
surface = None
clock = None

map = None
camera = None
player = None
playerVel = None
playerOnFloor = None
playerJumpProgress = None
playerHealth = None
playerPoints = None
playerPowerups = None
startTime = None

switchPowerup = None
coinGalorePowerup = None
invinciblePowerup = None
flyPowerup = None
megacoinPowerup = None
megabadthingPowerup = None

seed = None

trackID = 0

def get_badthing_chance(x):
    return min(x/50, 100)

def create_coin_tile():
    if switchPowerup:
        return TILE_BADTHING
    else:
        return TILE_COIN

def create_badthing_tile():
    if switchPowerup:
        return TILE_COIN
    else:
        return TILE_BADTHING

class MapBit(object):
    def __init__(self, x):
        self.x = x
        self.tiles = [TILE_NONE] * (HEIGHT/TILE_SIZE)
    
    def generate(self):
        bottom = HEIGHT-TILE_SIZE*2
        top = TILE_SIZE*2
        
        self.tiles[0] = TILE_MAP
        v = int(((math.sin(self.x * 5.0))*0.5+0.5)*4)
        v += int(((math.sin(self.x * 2.5))*0.5+0.5)*4)
        v += int(((math.sin(self.x))*0.5+0.5)*4)
        v /= 3
        for y in xrange(v):
            self.tiles[y+1] = TILE_MAP
        
        state = random.getstate()
        
        for i in xrange(1, 3):
            random.seed(self.x/[80, 160, 240][i-1] + i*100 + seed)
            y = random.randint(200, HEIGHT-TILE_SIZE)
            self.tiles[y/TILE_SIZE] = TILE_MAP
            
            state2 = random.getstate()
            random.seed(self.x + y)
            if random.randint(0, 100) > 75 or coinGalorePowerup:
                pos = y/TILE_SIZE-random.randint(1, 4)
                if self.tiles[pos] == TILE_NONE: self.tiles[pos] = create_coin_tile()
            elif random.randint(0, 100) > 100-get_badthing_chance(self.x/TILE_SIZE):
                pos = y/TILE_SIZE-random.randint(1, 4)
                if self.tiles[pos] == TILE_NONE: self.tiles[pos] = create_badthing_tile()
            elif random.randint(0, 100) == 100:
                pos = y/TILE_SIZE-random.randint(1, 4)
                if self.tiles[pos] == TILE_NONE: self.tiles[pos] = TILE_POWERUP
            elif random.randint(0, 100) > 90 and megacoinPowerup:
                pos = y/TILE_SIZE-random.randint(1, 4)
                if self.tiles[pos] == TILE_NONE: self.tiles[pos] = TILE_MEGACOIN
            elif random.randint(0, 100) > 90 and megabadthingPowerup:
                pos = y/TILE_SIZE-random.randint(1, 4)
                if self.tiles[pos] == TILE_NONE: self.tiles[pos] = TILE_MEGABADTHING
            random.setstate(state2)
        
        random.setstate(state)
    
    def create_rect(self):
        return pygame.Rect(self.x, 0, TILE_SIZE, HEIGHT)

def draw_circle(surf, pos, r, color):
    try:
        pygame.gfxdraw.aacircle(surf, pos[0], pos[1], r, color)
        pygame.gfxdraw.filled_circle(surf, pos[0], pos[1], r, color)
    except:
        pygame.draw.circle(surf, color, pos, r)

def draw_polygon(surf, pos, points, color):
    try:
        pygame.gfxdraw.aapolygon(surf, [(p[0]+pos[0], p[1]+pos[1]) for p in points], color)
        pygame.gfxdraw.filled_polygon(surf, [(p[0]+pos[0], p[1]+pos[1]) for p in points], color)
        pygame.gfxdraw.aapolygon(surf, [(p[0]+pos[0], p[1]+pos[1]) for p in points], color)
    except:
        pygame.draw.polygon(surf, color, [(p[0]+pos[0], p[1]+pos[1]) for p in points])
        for i in xrange(len(points)-1):
            pygame.draw.aaline(surf, color, (points[i][0]+pos[0], points[i][1]+pos[1]), (points[i+1][0]+pos[0], points[i+1][1]+pos[1]))
        pygame.draw.aaline(surf, color, (points[-1][0]+pos[0], points[-1][1]+pos[1]), (points[0][0]+pos[0], points[0][1]+pos[1]))

def draw_ngon(surf, count, pos, radius, color, orientation):
    draw_polygon(surf,
                 pos,
                 [(int(math.sin(i/float(count)*math.radians(360)+orientation)*radius),
                   int(math.cos(i/float(count)*math.radians(360)+orientation)*radius)) for i in xrange(count)],
                 color)

def draw_round_square(surf, pos, radius, color, topleft, topright, bottomleft, bottomright):
    corner_size = 2
    
    left = -radius
    right = radius
    top = -radius
    bottom = radius
    
    points = []
    
    if topleft:
        points += [(left+corner_size, top-1),
                   (right-1-corner_size, top-1)]
    else:
        points += [(left, top)]
    
    if topright:
        points += [(right-1, top-1+corner_size),
                   (right-1, bottom-1-corner_size)]
    else:
        points += [(right, top)]
    
    if bottomright:
        points += [(right-corner_size, bottom-1),
                   (left+corner_size, bottom-1)]
    else:
        points += [(right, bottom)]
    
    if bottomleft:
        points += [(left, bottom-1-corner_size),
                   (left, top-1+corner_size)]
    else:
        points += [(left, bottom)]
    
    draw_polygon(surf, pos, points, color)

def draw_text(dest, lines, color):
    surfaces = [font.render(line, True, color) for line in lines]
    
    y = (HEIGHT-sum([surf.get_height() for surf in surfaces])) / 2
    
    for surf in surfaces:
        dest.blit(surf, ((WIDTH-surf.get_width())/2, y))
        y += surf.get_height()

def get_item_orientation(bit, y):
    return time.time()-startTime + bit.x*math.radians(45) + y*math.radians(15)

def draw_map_bit(surf, bit, bitidx):
    y = 0
    for tile in bit.tiles:
        rect = pygame.Rect(bit.x, y, TILE_SIZE, TILE_SIZE)
        if tile == TILE_MAP:
            try: left = map[bitidx-1].tiles[y/TILE_SIZE]
            except IndexError: left = TILE_NONE
            try: right = map[bitidx+1].tiles[y/TILE_SIZE]
            except IndexError: right = TILE_NONE
            try: top = bit.tiles[y/TILE_SIZE-1]
            except IndexError: top = TILE_NONE
            try: bottom = bit.tiles[y/TILE_SIZE+1]
            except IndexError: bottom = TILE_NONE
            
            topleft = (top != TILE_MAP) and (left != TILE_MAP)
            topright = (top != TILE_MAP) and (right != TILE_MAP)
            bottomleft = (bottom != TILE_MAP) and (left != TILE_MAP)
            bottomright = (bottom != TILE_MAP) and (right != TILE_MAP)
            
            draw_round_square(surf,
                              rect.move(-camera.left, -camera.top).center,
                              rect.width/2,
                              (0, 0, 0),
                              topleft,
                              topright,
                              bottomleft,
                              bottomright)
        elif tile == TILE_COIN:
            pos = rect.move(-camera.left, -camera.top).center
            draw_ngon(surf, 5, pos, rect.width/2, (255, 255, 0), get_item_orientation(bit, y))
        elif tile == TILE_BADTHING:
            pos = rect.move(-camera.left, -camera.top).center
            draw_ngon(surf, 5, pos, rect.width/2, (255, 0, 0), get_item_orientation(bit, y))
        elif tile == TILE_POWERUP:
            pos = rect.move(-camera.left, -camera.top).center
            draw_ngon(surf, 5, pos, rect.width/2, (255, 255, 255), get_item_orientation(bit, y))
        y += TILE_SIZE

def draw_map(surf):
    toRemove = []
    i = 0
    
    for bit in map:
        y = 0
        for tile in bit.tiles:
            if tile == TILE_MEGACOIN:
                rect = pygame.Rect(bit.x, y, TILE_SIZE, TILE_SIZE)
                pos = rect.move(-camera.left, -camera.top).center
                draw_ngon(surf, 5, pos, TILE_SIZE, (255, 255, 0), get_item_orientation(bit, y))
            elif tile == TILE_MEGABADTHING:
                rect = pygame.Rect(bit.x, y, TILE_SIZE, TILE_SIZE)
                pos = rect.move(-camera.left, -camera.top).center
                draw_ngon(surf, 5, pos, TILE_SIZE, (255, 0, 0), get_item_orientation(bit, y))
            y += TILE_SIZE
    
    for bit in map:
        if bit.create_rect().colliderect(camera):
            draw_map_bit(surf, bit, i)
        else:
            toRemove.append(bit)
        i += 1
    
    for bit in toRemove:
        map.remove(bit)

def simulate():
    global playerVel, playerJumpProgress, playerOnFloor, player, playerPoints, playerHealth, playerPowerups
    
    playerOnFloor = False
    if playerJumpProgress >= 0.0:
        playerVel -= PLAYER_JUMP_VEL * (0.75 if flyPowerup else 1.0) * (1.0-playerJumpProgress)
        playerJumpProgress += 0.016
        if playerJumpProgress >= 1.0:
            playerJumpProgress = -1.0
    
    playerVel = min(playerVel+GRAVITY, MAX_VEL)
    
    def tilex(bit, y):
        global player
        tile = bit.tiles[y/TILE_SIZE]
        if tile != TILE_MAP:
            return
        
        rect = pygame.Rect(bit.x, y, TILE_SIZE, TILE_SIZE)
        if rect.colliderect(player):
            player.right = rect.left
    
    def tiley(bit, y):
        global playerVel, playerOnFloor
        tile = bit.tiles[y/TILE_SIZE]
        if tile != TILE_MAP:
            return
        
        rect = pygame.Rect(bit.x, y, TILE_SIZE, TILE_SIZE)
        if rect.colliderect(player):
            if playerVel < 0:
                player.top = rect.bottom
            elif playerVel > 0:
                player.bottom = rect.top
                playerOnFloor = True
            playerVel = 0
    
    for bit in map:
        for y in xrange(HEIGHT/TILE_SIZE):
            tile = bit.tiles[y]
            
            rect = pygame.Rect(bit.x, y*TILE_SIZE, TILE_SIZE, TILE_SIZE)
            if rect.colliderect(player):
                if tile == TILE_COIN:
                    coinSound.play()
                    playerPoints += 1
                    bit.tiles[y] = TILE_NONE
                elif tile == TILE_MEGACOIN:
                    coinSound.play()
                    coinSound.play()
                    coinSound.play()
                    coinSound.play()
                    playerPoints += 4
                    bit.tiles[y] = TILE_NONE
                elif tile == TILE_BADTHING:
                    if not invinciblePowerup:
                        playerHealth -= 1
                    hitSound.play()
                    bit.tiles[y] = TILE_NONE
                elif tile == TILE_MEGABADTHING:
                    if not invinciblePowerup:
                        playerHealth -= 4
                    hitSound.play()
                    hitSound.play()
                    hitSound.play()
                    hitSound.play()
                    bit.tiles[y] = TILE_NONE
                elif tile == TILE_POWERUP:
                    coinSound.play()
                    playerPowerups += 1
                    bit.tiles[y] = TILE_NONE
    
    player.x += PLAYER_SPEED
    for bit in map:
        for y in xrange(HEIGHT/TILE_SIZE):
            tilex(bit, y*TILE_SIZE)
    
    player.y += playerVel
    for bit in map:
        for y in xrange(HEIGHT/TILE_SIZE):
            tiley(bit, y*TILE_SIZE)

def do_powerup():
    global switchPowerup, coinGalorePowerup, invinciblePowerup, flyPowerup, megacoinPowerup, megabadthingPowerup
    
    powerup = random.randint(0, POWERUP_MAX-1)
    
    if powerup == POWERUP_SWITCH:
        switchPowerup = True
        pygame.time.set_timer(EVENT_DEACTIVATE_SWITCH, 5000)
    elif powerup == POWERUP_COIN_GALORE:
        coinGalorePowerup = True
        pygame.time.set_timer(EVENT_DEACTIVATE_COIN_GALORE, 10000)
    elif powerup == POWERUP_INVINCIBLE:
        invinciblePowerup = True
        playerHealth = MAX_PLAYER_HEALTH
        pygame.time.set_timer(EVENT_DEACTIVATE_INVINCIBLE, 10000)
    elif powerup == POWERUP_FLY:
        flyPowerup = True
        pygame.time.set_timer(EVENT_DEACTIVATE_FLY, 10000)
    elif powerup == POWERUP_MEGACOIN:
        megacoinPowerup = True
        pygame.time.set_timer(EVENT_DEACTIVATE_MEGACOIN, 10000)
    elif powerup == POWERUP_MEGABADTHING:
        megabadthingPowerup = True
        pygame.time.set_timer(EVENT_DEACTIVATE_MEGABADTHING, 10000)
    
    powerupSound.play()

def handle_event(event):
    global trackID
    
    if event.type == EVENT_MUSIC_END:
        pygame.mixer.music.queue(TRACKS[trackID])
        trackID += 1
        trackID %= len(TRACKS)

def play():
    global playerJumpProgress, playerPoints, playerHealth, playerPowerups, switchPowerup, coinGalorePowerup, invinciblePowerup, playerVel, map, player, camera, playerOnFloor, seed, startTime, flyPowerup, megacoinPowerup, megabadthingPowerup
    
    map = []
    camera = pygame.Rect(0, 0, WIDTH, HEIGHT)
    player = pygame.Rect(0, 0, TILE_SIZE, TILE_SIZE)
    playerVel = 0
    playerOnFloor = False
    playerJumpProgress = -1.0
    playerHealth = MAX_PLAYER_HEALTH
    playerPoints = 0
    playerPowerups = 0
    
    switchPowerup = False
    coinGalorePowerup = False
    invinciblePowerup = False
    flyPowerup = False
    megacoinPowerup = False
    megabadthingPowerup = False
    
    seed = time.time()
    startTime = time.time()
    
    running = True
    
    for x in xrange(WIDTH/TILE_SIZE):
        bit = MapBit(x*TILE_SIZE)
        bit.generate()
        map.append(bit)
    
    found = False
    for bit in map:
        for y in range(len(bit.tiles)-2):
            if bit.tiles[y] == TILE_NONE and \
               bit.tiles[y+1] == TILE_NONE and \
               bit.tiles[y+2] == TILE_MAP:
                player.left = bit.x
                player.top = y*TILE_SIZE
                found = True
                break
        
        if found:
            break
    
    if not found:
        return STATE_PLAY
    
    fade_start = time.time()
    paused = False
    
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                return STATE_QUIT
            elif event.type == EVENT_DEACTIVATE_SWITCH:
                switchPowerup = False
            elif event.type == EVENT_DEACTIVATE_COIN_GALORE:
                coinGalorePowerup = False
            elif event.type == EVENT_DEACTIVATE_INVINCIBLE:
                invinciblePowerup = False
            elif event.type == EVENT_DEACTIVATE_FLY:
                flyPowerup = False
            elif event.type == EVENT_DEACTIVATE_MEGACOIN:
                megacoinPowerup = False
            elif event.type == EVENT_DEACTIVATE_MEGABADTHING:
                megabadthingPowerup = False
            elif event.type == KEYDOWN:
                if event.key == QUIT_TO_MENU_KEY and paused:
                    return STATE_MENU
                elif event.key == POWERUP_KEY:
                    if playerPowerups > 0:
                        playerPowerups -= 1
                        do_powerup()
                elif event.key == PAUSE_KEY:
                    paused = not paused
            else:
                handle_event(event)
        
        fade_amount = 1.0 - pow(time.time()-fade_start, 2)
        
        if fade_amount <= 0 and not paused:
            if pygame.key.get_pressed()[JUMP_KEY] and (playerOnFloor or flyPowerup):
                if not flyPowerup: jumpSound.play()
                playerJumpProgress = 0.0
            
            simulate()
            
            if player.top > HEIGHT:
                if invinciblePowerup:
                    playerVel = -MAX_VEL
                    player.top = HEIGHT
                else:
                    playerHealth = 0
            
            if playerHealth <= 0:
                loseSound.play()
                return STATE_LOSE
        
        camera.left = player.left - WIDTH*0.25
        camera.left = max(camera.left, 0)
        
        while camera.right > map[-1].x+TILE_SIZE:
            bit = MapBit(map[-1].x+TILE_SIZE)
            bit.generate()
            map.append(bit)
        
        surface.fill((BACKGROUND, BACKGROUND, BACKGROUND))
        
        draw_map(surface)
        
        draw_round_square(surface,
                          player.move(-camera.left, -camera.top).center,
                          player.width/2,
                          (255, 0, 255),
                          True,
                          False,
                          True,
                          True)
        
        points_text = font.render("Score: %d" % playerPoints, True, (255, 255, 255))
        surface.blit(points_text, (WIDTH-points_text.get_width(), 0))
        
        health_text = font.render("Health: %.0f%s" % (playerHealth/float(MAX_PLAYER_HEALTH)*100.0, "%"), True, (255, 255, 255))
        surface.blit(health_text, (WIDTH-health_text.get_width(), points_text.get_height()))
        
        powerup_text = font.render("Effects: %d" % (playerPowerups), True, (255, 255, 255))
        surface.blit(powerup_text, (WIDTH-powerup_text.get_width(), points_text.get_height()+health_text.get_height()))
        
        if fade_amount > 0.0:
            v = 255 - int(fade_amount * 255)
            surface.fill((v, v, v), None, BLEND_MULT)
        
        if paused:
            surface.fill((127, 127, 127), None, BLEND_MULT)
            draw_text(surface,
                      ["Press %s to continue" % pygame.key.name(PAUSE_KEY),
                       "Press %s to stop playing" % pygame.key.name(QUIT_TO_MENU_KEY)],
                      (255, 255, 255))
        
        pygame.display.flip()
        
        pygame.display.set_caption("Frametime: %d ms" % clock.tick(60))

def lose():
    running = True
    
    start = time.time()
    
    while running:
        for event in pygame.event.get():
            if event.type == QUIT:
                return STATE_QUIT
            elif event.type == KEYDOWN:
                if int(3.0 - (time.time()-start)) <= 0:
                    return STATE_MENU
            else:
                handle_event(event)
        
        timeleft = int(3.0 - (time.time()-start))
        
        if timeleft <= 0:
            text = "Press any key"
        else:
            text = "Press any key in %d seconds" % (timeleft)
        
        surface.fill((0, 0, 0))
        
        draw_text(surface, ["You lost", "You score: %d" % playerPoints, text], (255, 255, 255))
        
        pygame.display.flip()
        
        pygame.display.set_caption("Frametime: %d ms" % clock.tick(15))

def menu():
    running = True
    
    while running:
        for event in pygame.event.get():
            if event.type == QUIT:
                return STATE_QUIT
            elif event.type == KEYDOWN:
                return STATE_PLAY
            else:
                handle_event(event)
        
        surface.fill((0, 0, 0))
        
        draw_text(surface,
                  ["Avoid red stuff",
                   "Get yellow stuff",
                   "White stuff are effects",
                   "Don't fall",
                   "Press %s to jump" % pygame.key.name(JUMP_KEY),
                   "Press %s to use a effect" % pygame.key.name(POWERUP_KEY),
                   "Press %s to pause" % pygame.key.name(PAUSE_KEY),
                   "Press any key to play",
                   "Settings are in settings.py"],
                  (255, 255, 255))
        
        pygame.display.flip()
        
        pygame.display.set_caption("Frametime: %d ms" % clock.tick(15))

def main():
    global coinSound, hitSound, jumpSound, loseSound, powerupSound, font, surface, clock
    
    pygame.init()
    
    font = pygame.font.Font("fonts/Orbitron Medium.otf", 36)
    
    pygame.mixer.set_num_channels(16)
    
    pygame.mixer.music.set_volume(MUSIC_VOLUME / 100.0)
    
    pygame.mixer.music.load(TRACKS[0])
    pygame.mixer.music.queue(TRACKS[1])
    pygame.mixer.music.play()
    pygame.mixer.music.set_endevent(EVENT_MUSIC_END)
    
    pygame.display.set_mode((WIDTH, HEIGHT))
    
    surface = pygame.display.get_surface()
    clock = pygame.time.Clock()
    
    running = True
    state = STATE_MENU
    
    coinSound = pygame.mixer.Sound("audio/coin.ogg")
    coinSound.set_volume(SOUND_EFFECT_VOLUME / 100.0)
    hitSound = pygame.mixer.Sound("audio/hit.ogg")
    hitSound.set_volume(SOUND_EFFECT_VOLUME / 100.0)
    jumpSound = pygame.mixer.Sound("audio/jump.ogg")
    jumpSound.set_volume(SOUND_EFFECT_VOLUME / 100.0)
    loseSound = pygame.mixer.Sound("audio/lose.ogg")
    loseSound.set_volume(SOUND_EFFECT_VOLUME / 100.0)
    powerupSound = pygame.mixer.Sound("audio/powerup.ogg")
    powerupSound.set_volume(SOUND_EFFECT_VOLUME / 100.0)
    
    while state != STATE_QUIT:
        if state == STATE_PLAY:
            state = play()
        elif state == STATE_LOSE:
            state = lose()
        elif state == STATE_MENU:
            state = menu()
        elif state == STATE_QUIT:
            pass
    
    pygame.quit()

if __name__ == "__main__":
    main()
