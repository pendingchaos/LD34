# ----- Change key bindings here -----
# See http://www.pygame.org/docs/ref/key.html for available keys
JUMP_KEY = K_w
POWERUP_KEY = K_e
PAUSE_KEY = K_q
QUIT_TO_MENU_KEY = K_e

# ----- Change resolution settings here -----
# These will be modified to be a multiple of TILE_SIZE
WIDTH = 960
HEIGHT = 540

# ----- Change audio settings here -----
TRACKS = ["audio/bu-eating-dwarves.ogg",
          #"audio/bu-gardens-and-a-garden.ogg",
          #"audio/bu-the-hunting-road.ogg",
          #"audio/bu-the-matrixs-tube.ogg",
          #"audio/bu-the-purple-gorilla.ogg",
          "audio/bu-the-wizards-toasters.ogg"]

# 0 to 100
MUSIC_VOLUME = 100
SOUND_EFFECT_VOLUME = 100
